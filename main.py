import random
import sys
import pygame
from pygame.locals import *
import json

# Global Variables for the game
FPS = 32
SCREENWIDTH = 289
SCREENHEIGHT = 511
SCREEN = pygame.display.set_mode((SCREENWIDTH, SCREENHEIGHT))
GROUNDY = int(SCREENHEIGHT * 0.8)
GAME_SPRITES = {}
GAME_SOUNDS = {}
PLAYER = 'gallery/sprites/bird.png'
BACKGROUND = 'gallery/sprites/background.png'
PIPE = 'gallery/sprites/pipe.png'

def welcomeScreen():
    """
    Shows welcome Images on Screen
    """
    player_x = int(SCREENWIDTH/5)
    player_y = int((SCREENHEIGHT - GAME_SPRITES['player'].get_height())/2)
    message_x = int((SCREENHEIGHT - GAME_SPRITES['message'].get_height())/4)
    message_y = int(SCREENHEIGHT * 0.13)
    base_x = 0
    while True:
        for event in pygame.event.get():
            # Close game on cross click
            if event.type == pygame.QUIT or (event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE):
                pygame.quit()
                sys.exit()
            elif event.type == pygame.KEYDOWN and (event.key == pygame.K_SPACE or event.key == pygame.K_UP) :
                return
        SCREEN.blit(GAME_SPRITES['background'], (0, 0))
        SCREEN.blit(GAME_SPRITES['player'], (player_x, player_y))
        SCREEN.blit(GAME_SPRITES['message'], (message_x, message_y))
        SCREEN.blit(GAME_SPRITES['base'], (base_x, GROUNDY))
        pygame.display.update()
        FPSCLOCK.tick(FPS)

def mainGame():
    score = 0
    player_x = int(SCREENWIDTH/5)
    player_y = int((SCREENHEIGHT - GAME_SPRITES['player'].get_height())/2)
    base_x = 0
    player_status = {}
    try:
        with open ('highScore.txt', 'r') as fin:
            player_status = json.loads(fin.read())
    except:
        with open ('highScore.txt', 'w') as fin:
            player_status = {
                "score": 0,
                "level": 1
            }
            fin.write(json.dumps(player_status))
    # Create 2 pipes bliting oh screen
    newPipe1 = getRandomPipe()  
    newPipe2 = getRandomPipe()
    # list of upper pipes
    upperPipes = [
        {'x': SCREENWIDTH+200, 'y': newPipe1[0]['y']},
        {'x': SCREENWIDTH+200+int(SCREENWIDTH/2), 'y': newPipe2[0]['y']}
    ]

    # list of upper pipes
    lowerPipes = [
        {'x': SCREENWIDTH+200, 'y': newPipe1[1]['y']},
        {'x': SCREENWIDTH+200+int(SCREENWIDTH/2), 'y': newPipe2[1]['y']}
    ]

    pipeVelX = -4

    playerVelY = -9
    playerMaxVelY = 10
    playerMinVelY = -8
    playerAccY = 1

    playerFlapAccv = -8 # velocity while flapping
    playerFlapped = False # It is True only when the bird is flapping
    while True:
        for event in pygame.event.get():
            # Close game on cross click
            if event.type == pygame.QUIT or (event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE):
                pygame.quit()
                sys.exit()
            elif event.type == pygame.KEYDOWN and (event.key == pygame.K_SPACE or event.key == pygame.K_UP) :
                if player_y > 0:
                    playerVelY = playerFlapAccv
                    playerFlapped = True
                    GAME_SOUNDS['wing'].play()

        crashTest = isCollide(player_x, player_y, upperPipes, lowerPipes) # this will return True if playes is crashed
        if crashTest:
            if score > int(player_status['score']):
                player_status['score'] = score
            with open('highScore.txt', 'w') as f:
                f.write(json.dumps(player_status))
            return

        # check for score
        playerMidPos = player_x + int(GAME_SPRITES['player'].get_width()/2)

        levelchange = 5 * (2 ** (int(player_status['level']) - 1))
        for pipe in upperPipes:
            pipeMidPos = pipe['x'] + int(GAME_SPRITES['pipe'][0].get_width()/2)
            if pipeMidPos <= playerMidPos < pipeMidPos + 4: 
                score += 1
                GAME_SOUNDS['point'].play()
                if score > levelchange:
                    player_status['level'] = int(player_status['level']) + 1
                    pipeVelX -= 1
                    levelchange *= 2 
        if playerVelY < playerMaxVelY and not playerFlapped:
            playerVelY += playerAccY

        if playerFlapped:
            playerFlapped = False
        
        # stop player in base ground
        playerHeight = GAME_SPRITES['player'].get_height()
        player_y = player_y + min(playerVelY, GROUNDY - player_y - playerHeight)

        # move pipe to left
        for lowerPipe, upperPipe in zip(upperPipes, lowerPipes):
            upperPipe['x'] += pipeVelX 
            lowerPipe['x'] += pipeVelX
        # Add a new Pipe when the first is about to cross the leftmost part of the screen
        if 0<upperPipes[0]['x']<5:
            newPipe = getRandomPipe()
            upperPipes.append(newPipe[0])
            lowerPipes.append(newPipe[1])
        
        # if the pipe is out of screen, remove it
        if upperPipes[0]['x'] < -GAME_SPRITES['pipe'][0].get_width():
            upperPipes.pop(0) 
            lowerPipes.pop(0) 

        # lets blit our sprites now
        SCREEN.blit(GAME_SPRITES['background'], (0,0))
        for upperPipe, lowerPipe in zip(upperPipes, lowerPipes):
            SCREEN.blit(GAME_SPRITES['pipe'][0], (upperPipe['x'], upperPipe['y']))
            SCREEN.blit(GAME_SPRITES['pipe'][1], (lowerPipe['x'], lowerPipe['y']))
        SCREEN.blit(GAME_SPRITES['base'], (base_x, GROUNDY))
        SCREEN.blit(GAME_SPRITES['player'], (player_x, player_y))

        myDigits = [int(x) for x in list(str(score))]
        width = 0
        for digit in myDigits:
            width += GAME_SPRITES['numbers'][digit].get_width()
        Xoffset = int((SCREENWIDTH - width)/2)

        for digit in myDigits:
            SCREEN.blit(GAME_SPRITES['numbers'][digit], (Xoffset, int(SCREENWIDTH*0.12)))
            Xoffset += GAME_SPRITES['numbers'][digit].get_width()

        # level of game
        levelXOffset = int((SCREENWIDTH - GAME_SPRITES['level'].get_width()) * 0.05)
        SCREEN.blit(GAME_SPRITES['level'], (levelXOffset, int(SCREENWIDTH*0.05)))
        levelScoreXOffset = int(GAME_SPRITES['level'].get_width() + GAME_SPRITES['levelnumbers'][0].get_width())
        digits = [int(x) for x in list(str(player_status['level']))]
        for digit in digits:
            SCREEN.blit(GAME_SPRITES['levelnumbers'][digit], (levelScoreXOffset, int(SCREENWIDTH*0.05)))
            levelScoreXOffset += GAME_SPRITES['levelnumbers'][digit].get_width()

        # Show High Score
        levelXOffset = int((SCREENWIDTH - GAME_SPRITES['level'].get_width()) * 0.05)
        levelYOffset = int(SCREENHEIGHT - GAME_SPRITES['base'].get_height() - GAME_SPRITES['highScore'].get_height())
        SCREEN.blit(GAME_SPRITES['highScore'], (levelXOffset, levelYOffset))
        levelScoreXOffset = int(GAME_SPRITES['highScore'].get_width() + GAME_SPRITES['levelnumbers'][0].get_width())
        digits = [int(x) for x in list(str(player_status['score']))]
        for digit in digits:
            SCREEN.blit(GAME_SPRITES['levelnumbers'][digit], (levelScoreXOffset, levelYOffset))
            levelScoreXOffset += GAME_SPRITES['levelnumbers'][digit].get_width()
        
        
        pygame.display.update()
        FPSCLOCK.tick(FPS)

def isCollide(player_x, player_y, upperPipes, lowerPipes):
    if player_y > GROUNDY - 25 or player_y < 0:
        GAME_SOUNDS['hit'].play()
        return True

    for pipe in upperPipes:
        pipeHeight = GAME_SPRITES['pipe'][0].get_height()
        if (player_y < pipeHeight + pipe['y'] and abs(player_x - pipe['x']) < GAME_SPRITES['pipe'][0].get_width()):
            GAME_SOUNDS['hit'].play()
            return True
    for pipe in lowerPipes:
        if (player_y + GAME_SPRITES['player'].get_height() > pipe['y'] and abs(player_x - pipe['x']) < GAME_SPRITES['pipe'][0].get_width()):
            GAME_SOUNDS['hit'].play()
            return True

    return False

def getRandomPipe(offsetRate=2):
    '''
    Generate positions of two pipes (one bottom and one upper pipe) for blitting on screen
    '''
    pipeHeight = GAME_SPRITES['pipe'][0].get_height()
    offset = int(SCREENWIDTH/offsetRate)
    y2 = offset + random.randrange(0, int(SCREENHEIGHT - GAME_SPRITES['base'].get_height() - int(1.2*offset)))
    pageX = SCREENWIDTH + 10
    y1 = int(pipeHeight - y2 + offset)
    pipe = [
        {'x': pageX, 'y': -y1}, # upper Pipe
        {'x': pageX, 'y': y2} # lower Pipe
    ] 
    return pipe


if __name__ == "__main__":
    # This will be the main point from where our game will start
    pygame.init() # initialize all pygame's module
    FPSCLOCK = pygame.time.Clock()
    pygame.display.set_caption("Flappy Bird By Priyam")

    # Game Images
    GAME_SPRITES['numbers'] = (
        pygame.image.load('gallery/sprites/0.png').convert_alpha(),
        pygame.image.load('gallery/sprites/1.png').convert_alpha(),
        pygame.image.load('gallery/sprites/2.png').convert_alpha(),
        pygame.image.load('gallery/sprites/3.png').convert_alpha(),
        pygame.image.load('gallery/sprites/4.png').convert_alpha(),
        pygame.image.load('gallery/sprites/5.png').convert_alpha(),
        pygame.image.load('gallery/sprites/6.png').convert_alpha(),
        pygame.image.load('gallery/sprites/7.png').convert_alpha(),
        pygame.image.load('gallery/sprites/8.png').convert_alpha(),
        pygame.image.load('gallery/sprites/9.png').convert_alpha()
    )
    GAME_SPRITES['levelnumbers'] = (
        pygame.image.load('gallery/sprites/L0.png').convert_alpha(),
        pygame.image.load('gallery/sprites/L1.png').convert_alpha(),
        pygame.image.load('gallery/sprites/L2.png').convert_alpha(),
        pygame.image.load('gallery/sprites/L3.png').convert_alpha(),
        pygame.image.load('gallery/sprites/L4.png').convert_alpha(),
        pygame.image.load('gallery/sprites/L5.png').convert_alpha(),
        pygame.image.load('gallery/sprites/L6.png').convert_alpha(),
        pygame.image.load('gallery/sprites/L7.png').convert_alpha(),
        pygame.image.load('gallery/sprites/L8.png').convert_alpha(),
        pygame.image.load('gallery/sprites/L9.png').convert_alpha()
    )
    GAME_SPRITES['message'] = pygame.image.load('gallery/sprites/message.png').convert_alpha()
    GAME_SPRITES['base'] = pygame.image.load('gallery/sprites/base.png').convert_alpha()
    GAME_SPRITES['pipe'] = (
        pygame.transform.rotate(pygame.image.load(PIPE).convert_alpha(), 180),
        pygame.image.load(PIPE).convert_alpha()
    )
    GAME_SPRITES['background'] = pygame.image.load(BACKGROUND).convert()
    GAME_SPRITES['player'] = pygame.image.load(PLAYER).convert_alpha()
    GAME_SPRITES['level'] = pygame.image.load('gallery/sprites/level.png').convert_alpha()
    GAME_SPRITES['highScore'] = pygame.image.load('gallery/sprites/highScore.png').convert_alpha()

    # Game Sounds
    GAME_SOUNDS['die'] = pygame.mixer.Sound('gallery/audio/die.wav')
    GAME_SOUNDS['hit'] = pygame.mixer.Sound('gallery/audio/hit.wav')
    GAME_SOUNDS['point'] = pygame.mixer.Sound('gallery/audio/point.wav')
    GAME_SOUNDS['swoosh'] = pygame.mixer.Sound('gallery/audio/swoosh.wav')
    GAME_SOUNDS['wing'] = pygame.mixer.Sound('gallery/audio/wing.wav')

    # GAME START
    while True:
        welcomeScreen() # For Welcome Screen
        mainGame() # Main game working Screen 